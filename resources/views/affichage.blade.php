<!DOCTYPE html>
<html>




</body>
</html>



@extends('layouts.layouts')

@section('content')


<!-- Page Content -->

   <!-- Portfolio Section -->

     <!-- Portfolio Grid-->
     <section class="page-section" id="portfolio">

<div class="text-center">
    <h2 > {{$lieux->nom_lieu}} </h2>
    <h3 class="section-subheading text-muted">Aménager votre intérieur de meuble plus unique les uns des autres.</h3>
</div>
  <div class="row">
  @foreach($allmeubles as $shop_meubles)
    <div class="col-lg-4 col-sm-6 mb-4">
        <div class="portfolio-item">
              <a class="portfolio-link" data-toggle="modal" href="#portfolioModal1">
                <div class="portfolio-hover">
                    <div class="portfolio-hover-content"></div>
                </div>
                <img class="img-fluid" src="{{ asset('img/'.$shop_meubles->images)}}" alt="" />
              </a>
            <div class="portfolio-caption ">
                <div class="portfolio-caption-heading " id="change">{{$shop_meubles->nom}}</div>
                <a href="{{url('/catalogue')}}"><button class="btn btn1  " id="bouton">Voir plus</button></a>
            </div>
        </div>
     </div>
     
      @endforeach
    <div class="col-lg-4 col-sm-6 mb-4">
        <div class="portfolio-item">
            <a class="portfolio-link" data-toggle="modal" href="#portfolioModal2">
                <div class="portfolio-hover">
                    <div class="portfolio-hover-content"></div>
                </div>
                <img class="img-fluid" src="img\JA.img\CH.jpg" alt="" />
            </a>
            <div class="portfolio-caption ">
                <div class="portfolio-caption-heading " id="change">Chaise</div>
                <a href="{{url('/catalogue')}}"><button class="btn btn1 " id="bouton">Voir plus</button></a>
            </div>
        </div>
      </div>
    <div class="col-lg-4 col-sm-6 mb-4">
        <div class="portfolio-item">
            <a class="portfolio-link" data-toggle="modal" href="#portfolioModal3">
                <div class="portfolio-hover">
                    <div class="portfolio-hover-content"></div>
                </div>
                <img class="img-fluid" src="img\JA.img\po.jpg" alt="" />
            </a>
            <div class="portfolio-caption ">
                <div class="portfolio-caption-heading " id="change">fleurs</div>
                <a href="{{url('/catalogue')}}"><button class="btn btn1 text-black " id="bouton">Voir plus</button></a>

            </div>
        </div>
    </div>
    <div class="col-lg-4 col-sm-6 mb-4 mb-lg-0">
        <div class="portfolio-item">
            <a class="portfolio-link" data-toggle="modal" href="#portfolioModal4">
                <div class="portfolio-hover">
                    <div class="portfolio-hover-content"></div>
                </div>
                <img class="img-fluid" src="img\JA.img\ta.jpg" alt="" />
            </a>
            <div class="portfolio-caption ">
                <div class="portfolio-caption-heading " id="change">Tapis</div>
                <a href="{{url('/catalogue')}}"><button class="btn btn1 text-black " id="bouton">Voir plus</button></a>

            </div>
        </div>
    </div>
    <div class="col-lg-4 col-sm-6 mb-4 mb-sm-0">
        <div class="portfolio-item">
            <a class="portfolio-link" data-toggle="modal" href="#portfolioModal5">
                <div class="portfolio-hover">
                    <div class="portfolio-hover-content"></div>
                </div>
                <img class="img-fluid" src="img\JA.img\lis.jpg" alt="" />
            </a>
            <div class="portfolio-caption ">
                <div class="portfolio-caption-heading" id="change">Lit</div>
                <a href="{{url('/catalogue')}}"><button class="btn btn1 text-black " id="bouton">Voir plus</button></a>

            </div>
        </div>
    </div>
    <div class="col-lg-4 col-sm-6">
        <div class="portfolio-item">
            <a class="portfolio-link" data-toggle="modal" href="#portfolioModal6">
                <div class="portfolio-hover">
                    <div class="portfolio-hover-content"></div>
                </div>
                <img class="img-fluid" src="img\JA.img\tm.jpg" alt="" />
            </a>
            <div class="portfolio-caption ">
                <div class="portfolio-caption-heading" id="change">Table à manger</div>
                <a href="{{url('/catalogue')}}"><button class="btn btn1 text-black " id="bouton" >Voir plus</button></a>
                
            </div>
        </div>
    </div>
</div>

</section>





  <!-- /.row -->



<!-- /.container -->


@endsection
