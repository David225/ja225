@extends('layouts.layouts')

@section('content')

<style>

</style>

<!-- Page Content -->
<div class="container " id="co">

  

  <div class="row">

     
  <div class="col-lg-3">

      <h1 class="my-4 ">Nos Meubles</h1>

   <div class="accordion">
     <div class="accordion-item" id="cate">
       <div class="accordion-item-header  active"   onclick="filterSelection('all')" id="text-dark">
          <span > Categories</span>
      </div>
    <div class="accordion-item-body">
  <div class="accordion" >
    @foreach($lieux as $lieu)
      <div class="accordion-item">
        <div class="accordion-item-header  "  >
         {{$lieu->nom_lieu}}
        </div>
        <div class="accordion-item-body">
          <div class="accordion-item-body-content">
          @foreach($categories as $category)
            @if($lieu->id == $category->LieuID)
          <ul> <li> <bouton class="btn"  onclick="filterSelection('{{$category->CatID}}')">{{$category->categorie}}</bouton> </li> </ul>
            @endif
          @endforeach
          </div>
        </div>
      </div>
    @endforeach
    </div>
  </div>    
</div>    
</div>
  </div>
    <!-- /.col-lg-3 -->

    <div class="col-lg-9">
      <div class="row ">
      @foreach($allmeubles as $shop_meubles)
        <div class="filterDiv col-lg-6 mb-4 {{$shop_meubles->categories_meubles_id}}">
          <div class=" h-100">
            <a href="article/{{$shop_meubles->id}}"><img class="card-img-top" src="{{ asset('img/'.$shop_meubles->images)}}" alt=""></a>
              
            <div class="card-body">
              <h4 class="card-title" style="text-align: center">
                <a class="lien" href="article/{{$shop_meubles->id}}">{{$shop_meubles->nom}}</a>
              </h4>
              <h5 class="prix" style="text-align: center">{{$shop_meubles->prix}} Francs CFA</h5>
              <div class="d-flex justify-content-between align-items-center">     
                </div>
              <hr class="trai">
            </div>
          </div>
        </div>
        @endforeach

      </div>
      <!-- /.row -->

    </div>
    <!-- /.col-lg-9 -->

  </div>
  <!-- /.row -->

</div>
<!-- /.container -->


@endsection
