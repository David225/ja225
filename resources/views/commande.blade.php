


@extends('layouts.layouts')

@section('content')
<style>
  #commande{
    position: relative!important;
    font-size: 7vw !important;
    color: #EEEEC6 !important;
    text-transform: uppercase !important;
    font-weight: 700 !important;
 
  }
  #commandeid{
    
    position: relative!important;
    font-size: 5vw !important;
    color:#fed136 !important;
    text-transform: uppercase !important;
    font-weight: 700 !important;
 
  }
        #prix{
          
    position: relative!important;
    font-size: 4vw !important;
    color:#EEEEC6 !important;
    text-transform: uppercase !important;
    font-weight: 400 !important;
        }
</style>


    
    <div class="container">
      <div class="row">
        <div class="col-lg-12 ">
        <div class="text-center">
          <h2 id="commande" >DEMANDE D'ACHAT </h2>
         </div>
          </div>
        </div>
      </div>
    </div>

<hr class="divi">
  <!-- Page Content -->
  
        <div class="text-center">
        
        
  <h3 class="my-3" id="commandeid">{{$artmeubles->categorie}}</h3>
  <span  id="prix" > <strong> {{$artmeubles->prix}} </strong> </span id="art">Francs CFA
  </div>

     <div class="container" id="conter">
    <div class="row">

      <div class="col-lg-8 col-md-10 mx-auto">
        <p class="test">Vous souhaitez éffectuer une demande d'achat de l'article : <span>{{$artmeubles->nom}}</span> Remplissez le formulaire ci-dessous afin de confirmer votre commande et nous vous repondrons pour la validation de votre commande dans les plus brefs délais!</p>
      
        <form method="post" action="{{ url('commande') }}">
           @csrf
           <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label>Meuble choisie</label>
              <select name="meubles_id" id="meubles_id" class="form-control @error('meubles_id') is-invalid @enderror" aria-describedby="inputGroupPrepend" required> 
              <option value="{{ $artmeubles->id_meubles }}">{{$artmeubles->nom}}</option>
              </select>
              <p class="help-block text-danger"></p>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label>Nom</label>
              <input type="text" class="form-control" placeholder="Nom" id="nom" required data-validation-required-message="Please enter your name." name="nom" >
              <p class="help-block text-danger"></p>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label>Pénom</label>
              <input type="text" class="form-control" placeholder="Prénom" id="prenom" required data-validation-required-message="Please enter your name."name="prenom">
              <p class="help-block text-danger"></p>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group col-xs-12 floating-label-form-group controls">
              <label>Numéro de téléphone</label>
              <input type="tel" class="form-control" placeholder="Numero de Téléphone" id="	contactes" required data-validation-required-message="Please enter your phone number."name="contactes">
              <p class="help-block text-danger"></p>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label>Addresse Email</label>
              <input type="email" class="form-control" placeholder=" Addresse Email" id="email" required data-validation-required-message="Please enter your email address." name="email">
              <p class="help-block text-danger"></p>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label>Message</label>
              <textarea rows="5" class="form-control" placeholder="Message" id="message" required data-validation-required-message="Please enter a message." name="message" ></textarea>
              <p class="help-block text-danger"></p>
            </div>
          </div>
          <br>
          <div id="success"></div>
          <button type="submit" class="btn btn1 "  id="bouton">Envoyer</button>
        </form>
      </div>
    </div>
  </div>
    <!-- Portfolio Item Row -->
    <div class="row">

     
   
   </div>
  
   
    
    <!-- /.row -->
</div>
  <!-- /.container -->
  @endsection

  <!-- Main Content -->
  
