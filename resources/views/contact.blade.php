@extends('layouts.layouts')

@section('content')

<style>
.page{
  margin-right:200px !important;
}

</style>

        <!-- Success message -->
        @if(Session::has('success'))
        <div class="alert alert-success">
            {{Session::get('success')}}
        </div>
        @endif


</div>
<header class="masthead" style="background-image: url('img/pawel.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="page">
            <h1 class="contac">Contactez Moi</h1>
            <span class="subheading">Vous avez des question ? J'ai des reponses.</span>
          </div>
        </div>
      </div>
    </div>
  </header>

  <!-- Main Content -->
  <div class="container" id="conter">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <p class="test">Vous souhaitez nous contacter? Remplissez le formulaire ci-dessous pour m'envoyer un message et je vous répondrai dans les plus brefs délais!</p>
        <!-- Contact Form - Enter your email address on line 19 of the mail/contact_me.php file to make this form work. -->
        <!-- WARNING: Some web hosts do not allow emails to be sent through forms to common mail hosts like Gmail or Yahoo. It's recommended that you use a private domain email address! -->
        <!-- To use the contact form, your site must be on a live web host with PHP! The form will not work locally! -->
        <form method="post" action="{{ url('contact') }}" action="{{ action('ContactController@store') }}">
          @csrf
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label>Nom</label>
              <input type="text" class="form-control" placeholder="Nom" id="nom" required data-validation-required-message="Please enter your name." name="nom" >
              <p class="help-block text-danger"></p>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label>Pénom</label>
              <input type="text" class="form-control" placeholder="Prénom" id="prenom" required data-validation-required-message="Please enter your name."name="prenom">
              <p class="help-block text-danger"></p>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label>Addresse Email</label>
              <input type="email" class="form-control" placeholder=" Addresse Email" id="email" required data-validation-required-message="Please enter your email address." name="email">
              <p class="help-block text-danger"></p>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group col-xs-12 floating-label-form-group controls">
              <label>Numéro de téléphone</label>
              <input type="tel" class="form-control" placeholder="Numero de Téléphone" id="phone" required data-validation-required-message="Please enter your phone number."name="phone">
              <p class="help-block text-danger"></p>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label>Message</label>
              <textarea rows="5" class="form-control" placeholder="Message" id="message" required data-validation-required-message="Please enter a message." name="message" ></textarea>
              <p class="help-block text-danger"></p>
            </div>
          </div>
          <br>
          <div id="success"></div>
          <button type="submit" class="btn btn1 "  id="bouton">Envoyer</button>
        </form>
      </div>
    </div>
  </div>

  

@endsection



