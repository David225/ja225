@extends('layouts.layouts')

@section('content')

<style>



</style>

<!-- Page Content -->
<div class="container" id="conte">

  <!-- Page Heading/Breadcrumbs -->

  

  <!-- Intro Content -->
  <div class="row">
    <div class="col-lg-6">
      <img class="img-fluid rounded mb-4" src="img/ja (1).jpg" alt="">
    </div>
    <div class="col-lg-6">
    <strong>  <h2 id="elle">Qui sommes nous ? </h2> </strong>
      <p class="lui">Joele Agouassi JA est une marque  créé  à Abidjan en 2017 par une petite équipe animée par le goût d’entreprendre. Dès les origines, nous souhaitions maîtriser tous les maillons de la chaîne pour proposer à nos clients des meubles qualitatifs et durables au meilleur prix.</p>

    </div>
  </div>
  <!-- /.row -->
<hr class="trai">   <!-- Team Members -->


  <div class="row">
    <div class="col-lg-6 mb-4">
      <div class="h-100 text-center">
        <img class="card-img-top" src="img/dist.png" alt="">
        <div class="card-body">
          <h4 class="card-title text-warning">Notre distribution</h4>
          <h6 class="card-subtitle mb-2 text-muted">servir et satisfaire nos clients</h6>
          <p class="card-text  " id="text-white">Afin de pouvoir proposer le meilleur rapport qualité/prix, nous avons fait le choix de distribuer nos produits <br>Nos livraisons s'effectuent même le soir et le week-end : sur rendez-vous du lundi au dimanche de 7:00 à 22:00. </p>
        </div>
        <hr class="trai">
       
      </div>
    </div>
    <div class="col-lg-6 mb-4">
      <div class=" h-100 text-center">
        <img class="card-img-top" src="img/bois.jpg" alt="">
        <div class="card-body">
          <h4 class="card-title text-warning">Authenticité des produits</h4>
          <h6 class="card-subtitle mb-2 text-muted">Des preuves plus que des beaux discours</h6>
          <p class="card-text " id="text-white">Nous avons fait le choix du bois massif et de la production artisanale. Nous travaillons sans intermédiaire. Les produits que nous proposons à la vente sont directement issus des ateliers de nos partenaires artisans.</p>
        </div>
        <hr class="trai">
       
      </div>
    </div>
    
    

    <div class="col-lg-6 mb-4">
      <div class=" h-100 text-center">
        <img class="card-img-top" src="img/idea.jpg" alt="">
        <div class="card-body">
          <h4 class="card-title text-warning">Notre esprit createur</h4>
          <h6 class="card-subtitle mb-2 text-muted">Des preuves plus que des beaux discours</h6>
          <p class="card-text " id="text-white">Créer, dessiner et mettre en scène les produits les plus mémorables pour vivre vos plus beaux moments, voici ce qui nous anime par-dessus tout. Nos meubles sont imaginés pour accompagner vos histoires de vie, longtemps.</p>
        </div>
        <hr class="trai">
       

      </div>
      

    </div>
    <div class="col-lg-6 mb-4">
      <div class=" h-100 text-center">
        <img class="card-img-top" src="img/hands.jpg" alt="">
        <div class="card-body">
          <h4 class="card-title text-warning">Nos garanties</h4>
          <h6 class="card-subtitle mb-2 text-muted">Des meubles en bois massif</h6>
          <p class="card-text " id="text-white">Parce qu’acheter un meuble est une affaire sérieuse, que nous ne sommes pas de simples distributeurs mais bien des concepteurs-constructeurs, que nous vous vendons un bien durable et de qualité, que nous n’avons qu’une parole, que nous tenons nos engagements.</p>
        </div>
        <hr class="trai">
       
      </div>
    </div>
   
  
    
  </div>
  <!-- /.row -->


  <!-- /.row -->

</div>

@endsection
