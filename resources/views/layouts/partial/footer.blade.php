<style>



</style>


<footer>
<div class="mt-5 pt-5 pb-5 footer">
<div class="container">
  <div class="row">
    <div class="col-lg-4 col-xs-12 about-company">
      <h3 class="un">  <strong> Joele Agouassi </strong></h3>
      
      <p class="deux">Suivez moi sur  </p>
      <a href="https://www.facebook.com/joeleMobilier/" class="fa fa-facebook" id="face"></a>
        <a href="https://www.instagram.com/joele.agouassi/" class="fa fa-instagram" id="insta"></a>

      </p>
    </div>
    <div class="col-lg-4 col-xs-12 links">
      <h4 class="mt-lg-0 mt-sm-3"> <strong> Liens </strong></h4>
      
        <ul class="m-0 p-0" id="stu">

          <li> <a href="{{url('/catalogue')}}" id="as">Salon</a></li>
          <li> <a href="{{url('/catalogue')}}" id="as">Chambre</a></li>
          <li> <a href="{{url('/catalogue')}}"  id="as">Salle à manger</a></li>
          <li> <a href="{{url('/apropos')}}"  id="as">A propos</a></li>
          <li> <a href="{{url('/contact')}}"  id="as">Contact</a></li>
        </ul>
    </div>
    <div class="col-lg-4 col-xs-12 location">
      <h4 class="mt-lg-0 mt-sm-4"> <strong> Retrouvez nous </strong></h4>
     
      <p>à Adjame 220 logement, <br>en face de Fraterniter Matin</p>
      <p class="mb-0"><i class="fa fa-phone mr-3" id="insta"></i>(+225) 07 09 94 94 15</p>
      <p><i class="fa fa-envelope mr-3" id="insta"></i>joeleatchui@gmail.com</p>


    </div>
  </div>
  <div class="row mt-5">
    <div class="col copyright">
      <p class=""><small class="text-white-50">© 2021. Made by Dsama.</small></p>
    </div>
  </div>
</div>
</div>
</footer>
