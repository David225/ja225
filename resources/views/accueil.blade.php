
@extends('layouts.layouts')

@section('content')

<style>



</style>
<div id="myCarousel" class="carousel slide" data-ride="carousel">
  
  <div class="carousel-inner ">
    <div class="carousel-item active ">
    <a href="{{url('/catalogue')}}"><img class="first-slide " src="img/JA.img/SA.jpg" alt="First slide" > </a>

      <div class="carousel-caption">
        <div class="carousel-caption text-center">
          <h1 class="firts">SALON </h1>
          <p class="carou">souhaitez-vous avoir un salon plein de style ? nos confections qui vous aideront à vous inspirer !</p>
       
        </div>
      </div>
    </div>
    <div class="carousel-item">
    <a href="{{url('/catalogue')}}"> <img class="second-slide" src="img/JA.img/SM.jpg" alt="Second slide" id=""> </a>
      <div class="carousel-caption">
        <div class="carousel-caption">
          <h1 class="second">SALLE A MANGER </h1>
          <p class="carou">Vous aimerez sa sobriété, idéale pour la coordonner à tout styles d'intérieur </p>
         
        </div>
      </div>
    </div>
    <div class="carousel-item">
    <a href="{{url('/catalogue')}}">  <img class="fourth-slide" src="img\JA.img\ACC.jpg" alt="Third  slide"> </a>
      <div class="carousel-caption">
        <div class="carousel-caption text-center">
          <h1 class="third">ACESSOIRE </h1>
          <p class="carou"> Nous vous proposons un large choix de décoration d’intérieur et d’extérieur haut de gamme et fait le point sur les dernières tendances en matière d’habitat.</p>
         
        </div>
      </div>
    </div>
    <div class="carousel-item">
    <a href="{{url('/catalogue')}}">  <img class="third-slide" src="img\JA.img\LI.jpg" alt="Fourth slide"></a>
      <div class="carousel-caption">
        <div class="carousel-caption text-center">
          <h1 class="fourth">CHAMBRE </h1>
          <p class="carou"> Authentique, racé, le bois est le matériau incontournable pour ménager une ambiance douillette et cocooning dans la chambre, en toute élégance.</p>
         
        </div>
      </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>


<!-- Page Content -->

   <!-- Portfolio Section -->

     <!-- Portfolio Grid-->
<section class="page-section" id="portfolio">

 <div class="text-center">
     <h2 >AKWABA </h2>
     <hr class="divi">
     <h3 class="section-subheading text-muted">Aménager votre intérieur de meuble plus unique les uns des autres.</h3>
     
 </div>
   <div class="row">
     <div class="col-lg-4 col-sm-6 mb-4">
         <div class="portfolio-item">
               <a class="portfolio-link" data-toggle="modal" href="#portfolioModal1">
                 <div class="portfolio-hover">
                     <div class="portfolio-hover-content"></div>
                 </div>
                 <img class="img-fluid" src="img\JA.img\mt.jpg" alt="" />
               </a>
             <div class="portfolio-caption ">
                 <div class="portfolio-caption-heading " id="change">Meuble TV</div>
                 <a href="{{url('/catalogue')}}"><button class="btn btn1  " id="bouton">Voir plus</button></a>
             </div>
         </div>
      </div>
     <div class="col-lg-4 col-sm-6 mb-4">
         <div class="portfolio-item">
             <a class="portfolio-link" data-toggle="modal" href="#portfolioModal2">
                 <div class="portfolio-hover">
                     <div class="portfolio-hover-content"></div>
                 </div>
                 <img class="img-fluid" src="img\JA.img\CH.jpg" alt="" />
             </a>
             <div class="portfolio-caption ">
                 <div class="portfolio-caption-heading " id="change">Chaise</div>
                 <a href="{{url('/catalogue')}}"><button class="btn btn1 " id="bouton">Voir plus</button></a>
             </div>
         </div>
       </div>
     <div class="col-lg-4 col-sm-6 mb-4">
         <div class="portfolio-item">
             <a class="portfolio-link" data-toggle="modal" href="#portfolioModal3">
                 <div class="portfolio-hover">
                     <div class="portfolio-hover-content"></div>
                 </div>
                 <img class="img-fluid" src="img\JA.img\po.jpg" alt="" />
             </a>
             <div class="portfolio-caption ">
                 <div class="portfolio-caption-heading " id="change">fleurs</div>
                 <a href="{{url('/catalogue')}}"><button class="btn btn1 text-black " id="bouton">Voir plus</button></a>

             </div>
         </div>
     </div>
     <div class="col-lg-4 col-sm-6 mb-4 mb-lg-0">
         <div class="portfolio-item">
             <a class="portfolio-link" data-toggle="modal" href="#portfolioModal4">
                 <div class="portfolio-hover">
                     <div class="portfolio-hover-content"></div>
                 </div>
                 <img class="img-fluid" src="img\JA.img\ta.jpg" alt="" />
             </a>
             <div class="portfolio-caption ">
                 <div class="portfolio-caption-heading " id="change">Tapis</div>
                 <a href="{{url('/catalogue')}}"><button class="btn btn1 text-black " id="bouton">Voir plus</button></a>

             </div>
         </div>
     </div>
     <div class="col-lg-4 col-sm-6 mb-4 mb-sm-0">
         <div class="portfolio-item">
             <a class="portfolio-link" data-toggle="modal" href="#portfolioModal5">
                 <div class="portfolio-hover">
                     <div class="portfolio-hover-content"></div>
                 </div>
                 <img class="img-fluid" src="img\JA.img\lis.jpg" alt="" />
             </a>
             <div class="portfolio-caption ">
                 <div class="portfolio-caption-heading" id="change">Lit</div>
                 <a href="{{url('/catalogue')}}"><button class="btn btn1 text-black " id="bouton">Voir plus</button></a>

             </div>
         </div>
     </div>
     <div class="col-lg-4 col-sm-6">
         <div class="portfolio-item">
             <a class="portfolio-link" data-toggle="modal" href="#portfolioModal6">
                 <div class="portfolio-hover">
                     <div class="portfolio-hover-content"></div>
                 </div>
                 <img class="img-fluid" src="img\JA.img\tm.jpg" alt="" />
             </a>
             <div class="portfolio-caption ">
                 <div class="portfolio-caption-heading" id="change">Table à manger</div>
                 <a href="{{url('/catalogue')}}"><button class="btn btn1 text-black " id="bouton" >Voir plus</button></a>

             </div>
         </div>
     </div>
 </div>

</section>





   <!-- /.row -->



 <!-- /.container -->

@endsection
