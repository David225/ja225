<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CommandesController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('accueil');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
Route::get('/accueil', 'AccueilController@index');
Route::get('/apropos', 'AproposController@index');

Route::get('/catalogue', 'CatalogueController@index');
//Route::get('/catalogue/{id}', 'CatalogueController@index');
Route::get('/article/{id}','CatalogueController@article');
Route::get('/article/commande/{id}','CatalogueController@commande');



Route::get('/contact', 'ContactController@index');
Route::get('/contact', 'ContactController@index');
Route::post('/contact', 'ContactController@store');

Route::get('/affichage','AffichagaController@index');
Route::get('/affichage/{id}','AffichagaController@article');

Route::get('/commande', 'CommandesController@index');
Route::post('/commande', 'CommandesController@store');


//Route::get('/commande/{id}', 'CatalogueController@article');

//Route::post('/affichage','AffichagaController@store');
//Route::get('laravel-form-validation-example', 'formValidationExample\FormValidationController@index');
//Route::post('laravel-form-validation-example', 'formValidationExample\FormValidationController@store');
