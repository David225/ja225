<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ArticleController extends Controller
{
 


  public function article(int $id)

  {
   $artmeubles = DB::table('meubles')
   ->where('meubles.id', '=', $id)
   ->join('categories_meubles', 'meubles.categories_meubles_id', '=', 'categories_meubles.id')
   ->join('joint_lieu', 'meubles.id', '=', 'joint_lieu.id_meubles')
   ->join('joint_matiere', 'meubles.id', '=', 'joint_matiere.id_meubles') 
   ->join('joint_couleur', 'meubles.id', '=', 'joint_couleur.id_meubles')
   ->join('couleur', 'joint_couleur.id_couleur', '=', 'couleur.id')
   ->join('lieu', 'joint_lieu.id_lieu', '=', 'lieu.id')
   ->join('matiere', 'joint_matiere.id_matiere', '=', 'matiere.id')
   ->get();
  
   if (count($artmeubles) == 0 ) {
     return view('errors.404');

   }else {
    
    return view('commande', ['artmeubles' => $artmeubles[0]]);
   }
  }                 

}
