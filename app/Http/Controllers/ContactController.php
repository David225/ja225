<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator, Redirect, Response, File;
use App\contacts;


class ContactController extends Controller
{
  public function index()
  {
    return view('contact');
  }
   
   // Store Form data in database
   public function store(Request $request) {
 
    // Form validation
    $this->validate($request, [
        'nom' => 'required',
        'prenom'=>'required',
        'email' => 'required|email',
        'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:8',
        'message' => 'required',
     ]);

     
    //  Store data in database
    'App\contacts'::create($request->all());
    //
    
    return back()->with('success', 'Les données ont été enregistrées avec succès.');
}



}