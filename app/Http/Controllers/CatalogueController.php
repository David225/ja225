<?php



namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;



class CatalogueController extends Controller
{
  public function index()
  {
    $allmeubles = DB::table('meubles')
                    //->join('categories_meubles', 'meubles.categories_meubles_id', '=', 'categories_meubles.id')
                   //->join('joint_matiere', 'meubles.id', '=', 'joint_matiere.id_meubles')
                    //->join('joint_couleur', 'meubles.id', '=', 'joint_couleur.id_meubles')
                    //->join('joint_lieu', 'meubles.id', '=', 'joint_lieu.id_meubles')
                    //->select( 'all') 
                    ->get();

     $lieux = DB::table('lieu')
                  ->select('nom_lieu','id')
                  ->get(); 

     $categories = DB::table('categories_meubles')
         //->where('liaison.LieuID', '=', 'liaison.CatID')
               ->join('liaison', 'liaison.CatID', '=', 'categories_meubles.id')
               ->join('lieu', 'liaison.LieuID', '=', 'lieu.id')
               ->get(); 
               
        return view('catalogue', compact('allmeubles','categories','lieux'));
      //dd($all);
     
      //return view('catalogue', ['all' => $all[0]]);
      // tout ce qui est en dessous doit être supprimé après les tests
      // $categories = $all;
      //return view('affichage', compact('categories','allmeubles'));   
      }

    




   public function article(int $id)

   {
    $artmeubles = DB::table('meubles')
    ->where('meubles.id', '=', $id)
    ->join('categories_meubles', 'meubles.categories_meubles_id', '=', 'categories_meubles.id')
    ->join('joint_lieu', 'meubles.id', '=', 'joint_lieu.id_meubles')
    ->join('joint_matiere', 'meubles.id', '=', 'joint_matiere.id_meubles') 
    ->join('joint_couleur', 'meubles.id', '=', 'joint_couleur.id_meubles')
    ->join('couleur', 'joint_couleur.id_couleur', '=', 'couleur.id')
    ->join('lieu', 'joint_lieu.id_lieu', '=', 'lieu.id')
    ->join('matiere', 'joint_matiere.id_matiere', '=', 'matiere.id')
    ->get();
        
    if (count($artmeubles) == 0 ) {
      return view('errors.404');

    }else {
     
     return view('article', ['artmeubles' => $artmeubles[0]]);
    }
                    
 }
        //dd($allmeubles);
        
  
        public function commande(int $id)

        {
         $artmeubles = DB::table('meubles')
         ->where('meubles.id', '=', $id)
         ->join('categories_meubles', 'meubles.categories_meubles_id', '=', 'categories_meubles.id')
         ->join('joint_lieu', 'meubles.id', '=', 'joint_lieu.id_meubles')
         ->join('joint_matiere', 'meubles.id', '=', 'joint_matiere.id_meubles') 
         ->join('joint_couleur', 'meubles.id', '=', 'joint_couleur.id_meubles')
         ->join('couleur', 'joint_couleur.id_couleur', '=', 'couleur.id')
         ->join('lieu', 'joint_lieu.id_lieu', '=', 'lieu.id')
         ->join('matiere', 'joint_matiere.id_matiere', '=', 'matiere.id')
         ->get();
        
         if (count($artmeubles) == 0 ) {
           return view('errors.404');
     
         }else {
          
          return view('commande', ['artmeubles' => $artmeubles[0]]);
         }
                         
       } 
       
       


}


  //public function article ($id)

//$meuble = DB::table('meubles')
  //                 where (meubles.id = $id)
//->join('categories_meubles', 'meubles.categories_meubles_id', '=', 'categories_meubles.id')
//->join('joint_matiere', 'meubles.id', '=', 'joint_matiere.id_meubles')
//->join('joint_couleur', 'meubles.id', '=', 'joint_couleur.id_meubles')
//->select( 'all') 
//->get();

