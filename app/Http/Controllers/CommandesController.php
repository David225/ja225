<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator, Redirect, Response, File;
use App\Commandes;




class CommandesController extends Controller
{
    public function index()
    {
      return view('commande');
    }
     
     // Store Form data in database
     public function store(Request $request)
      {

      // Form validation
      $this->validate($request, [
          'meubles_id' => 'required',
          'nom' => 'required',
          'prenom'=>'required',
          'contactes' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:12',
          'email' => 'required|email',
          'message' => 'required',
       ]);
      //  Store data in database
      'App\Commandes'::create($request->all());
      //
      
      return back()->with('success', 'Les données ont été enregistrées avec succès.');
  }
  
}
