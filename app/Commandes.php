<?php

namespace App;
use Notifiable; 
use Illuminate\Database\Eloquent\Model;

class Commandes extends Model
{
    //
    protected $table = 'Commandes';
    protected $fillable = [
        'meubles_id', 'nom', 'prenom','contactes','email',  'message', 
    ];

}
