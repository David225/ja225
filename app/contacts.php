<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Notifiable; 
class contacts extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'contacts';
    protected $fillable = [
        'nom', 'prenom', 'email', 'phone', 'message', 
    ];

}
