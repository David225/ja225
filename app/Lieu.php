<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Lieu extends Model
{
    //
    return $this->hasMany('App\Lieu');

    protected $lieux = 'nom_lieu';

}
