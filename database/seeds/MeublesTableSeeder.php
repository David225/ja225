<?php

use Illuminate\Database\Seeder;

class MeublesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()

    {
      
       
       $shop_meubles= new \App\Meubles();
       $shop_meubles->nom="Salle de bain";
       $shop_meubles->prix=150000;
       $shop_meubles->dimension="H55xL180xP45cm";
       $shop_meubles->categories_mobilier=5;
       $shop_meubles->description="Accessoire.";
       $shop_meubles->images="salle de bain.jpg";
       $shop_meubles->save();
       
       
       
       

       
    }
}
